﻿using System;

namespace webapi.Entities
{
    public record Item
    {
        public Guid Id { get; init; }
    }
}
